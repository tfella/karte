// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.coreaddons

import org.kde.karte

Kirigami.ApplicationWindow {
    id: root

    title: i18nc("@title", "Karte")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("Karte")
        titleIcon: "applications-graphics"
        isMenu: !Kirigami.Settings.isMobile
        actions: [
            Kirigami.Action {
                text: i18n("About Karte")
                icon.name: "help-about"
                onTriggered: root.pageStack.pushDialogLayer(Qt.createComponent("org.kde.kirigamiaddons.formcard", "AboutPage"), {}, {
                    title: i18nc("@title", "About")
                })
            },
            Kirigami.Action {
                text: i18n("Quit")
                icon.name: "application-exit"
                onTriggered: Qt.quit()
            }
        ]
    }

    pageStack.initialPage: Qt.createComponent("org.kde.karte", "MapPage")
}
