// SPDX-FileCopyrightText: 2024 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Controls as QQC2

import org.kde.kirigami as Kirigami

import org.kde.karte

Kirigami.Page {
    id: page

    title: i18nc("@title", "Karte")
    padding: 0
    MapView {
        anchors.fill: parent
    }
}
