// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <QtGlobal>
#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#endif

#include <QFile>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QUrl>

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "karteconfig.h"
#include "kartestate.h"
#include "version-karte.h"

#ifdef Q_OS_WINDOWS
#include <Windows.h>
#endif

#ifdef Q_OS_ANDROID
Q_DECL_EXPORT
#endif
int main(int argc, char *argv[])
{
#ifdef Q_OS_ANDROID
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle(QStringLiteral("org.kde.breeze"));
#else
    QApplication app(argc, argv);

    // Default to org.kde.desktop style unless the user forces another style
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }
#endif

#ifdef Q_OS_WINDOWS
    if (AttachConsole(ATTACH_PARENT_PROCESS)) {
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
    }

    QApplication::setStyle(QStringLiteral("breeze"));
    auto font = app.font();
    font.setPointSize(10);
    app.setFont(font);
#endif

    KLocalizedString::setApplicationDomain("karte");
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));

    KAboutData aboutData(QStringLiteral("karte"),
                         i18nc("@title", "Karte"),
                         QStringLiteral(KARTE_VERSION_STRING),
                         i18n("Map Viewer"),
                         KAboutLicense::GPL,
                         i18n("2024 KDE Community"));
    aboutData.addAuthor(i18nc("@info:credit", "Tobias Fella"),
                        i18nc("@info:credit", "Maintainer"),
                        QStringLiteral("tobias.fella@kde.org"),
                        QStringLiteral("https://tobiasfella.de"));
    aboutData.setTranslator(i18nc("NAME OF TRANSLATORS", "Your names"), i18nc("EMAIL OF TRANSLATORS", "Your emails"));
    KAboutData::setApplicationData(aboutData);
    QGuiApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("globe")));

    QQmlApplicationEngine engine;

    auto config = KarteConfig::self();
    auto state = KarteState::self();

    qmlRegisterSingletonInstance("org.kde.karte.config", 1, 0, "Config", config);
    qmlRegisterSingletonInstance("org.kde.karte.state", 1, 0, "State", state);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.loadFromModule("org.kde.karte", "Main");

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
